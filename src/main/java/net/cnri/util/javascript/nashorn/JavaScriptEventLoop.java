package net.cnri.util.javascript.nashorn;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import jdk.nashorn.api.scripting.JSObject;

public class JavaScriptEventLoop {
    static final ThreadLocal<JavaScriptEventLoop> threadLocalJavaScriptEventLoop = new ThreadLocal<>();
    private static final AtomicInteger threadNumber = new AtomicInteger(1);

    private final ScheduledExecutorService execServ;

    public JavaScriptEventLoop() {
        execServ = Executors.newScheduledThreadPool(1, r -> new Thread(r, "JavaScript-event-loop-" + threadNumber.getAndIncrement()));
        ((ScheduledThreadPoolExecutor)execServ).setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
        ((ScheduledThreadPoolExecutor)execServ).setRemoveOnCancelPolicy(true);
        try {
            execServ.submit(() -> threadLocalJavaScriptEventLoop.set(this)).get();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof RuntimeException) throw (RuntimeException)cause;
            if (cause instanceof Error) throw (Error)cause;
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public Future<?> submit(Runnable r) {
        return execServ.submit(r);
    }

    public <T> Future<T> submit(Callable<T> c) {
        return execServ.submit(c);
    }

    /**
     * Ensures that no tasks remain queued, but do not terminate
     */
    public void clear() throws InterruptedException {
        BlockingQueue<Runnable> queue = ((ThreadPoolExecutor)execServ).getQueue();
        try {
            execServ.submit(() -> queue.clear()).get();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof RuntimeException) throw (RuntimeException)cause;
            if (cause instanceof Error) throw (Error)cause;
            throw new RuntimeException(e);
        }
    }

    public void shutdown() {
        execServ.shutdown();
    }

    public Future<?> setImmediate(Object ueh, JSObject fn, Object... args) {
        return execServ.submit(handleExceptions(ueh, () -> fn.call(null, args)));
    }

    public Future<?> setTimeout(Object ueh, JSObject fn, long delay, Object... args) {
        return execServ.schedule(handleExceptions(ueh, () -> fn.call(null, args)), delay, TimeUnit.MILLISECONDS);
    }

    public Future<?> setInterval(Object ueh, JSObject fn, long delay, Object... args) {
        return execServ.scheduleWithFixedDelay(handleExceptions(ueh, () -> fn.call(null, args)), delay, delay, TimeUnit.MILLISECONDS);
    }

    private Runnable handleExceptions(Object ueh, Runnable runnable) {
        return () -> {
            try {
                runnable.run();
            } catch (Throwable t) {
                if (ueh instanceof Thread.UncaughtExceptionHandler) {
                    ((Thread.UncaughtExceptionHandler)ueh).uncaughtException(Thread.currentThread(), t);
                } else if (ueh instanceof JSObject) {
                    ((JSObject)ueh).call(null, Thread.currentThread(), t);
                } else {
                    t.printStackTrace();
                }
                throw t;
            }
        };
    }

}
