package net.cnri.util.javascript.nashorn;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.script.CompiledScript;
import javax.script.ScriptContext;
import javax.script.ScriptException;

import jdk.nashorn.api.scripting.JSObject;
import jdk.nashorn.api.scripting.NashornException;

public class ModuleSystem {
    private final ScriptEngineAndCompilationCache scriptEngineAndCompilationCache;
    private final ScriptContext scriptContext;
    private final JSObject moduleCache;
    private final RequireLookup requireLookup;

    public ModuleSystem(ScriptEngineAndCompilationCache scriptEngineAndCompilationCache, ScriptContext scriptContext, JSObject moduleCache, RequireLookup requireLookup) {
        this.scriptEngineAndCompilationCache = scriptEngineAndCompilationCache;
        this.scriptContext = scriptContext;
        this.moduleCache = moduleCache;
        this.requireLookup = requireLookup;
    }

    private static final String MODULE_PREFIX = "function (exports, require, module, __filename, __dirname) {";
    private static final String MODULE_SUFFIX = "\n}";

    private static String dirname(String s) {
        int lastSlash = s.lastIndexOf('/');
        if (lastSlash == -1) return ".";
        if (lastSlash == 0) return "/";
        return s.substring(0, lastSlash);
    }

    private static final String readFully(Reader reader) throws IOException {
        StringBuilder sb = new StringBuilder();
        char[] buf = new char[8192];
        int r;
        while ((r = reader.read(buf)) > 0) {
            sb.append(buf, 0, r);
        }
        return sb.toString();
    }

    private boolean moduleExists(String id) {
        if (moduleCache.hasMember(id)) return true;
        if (scriptEngineAndCompilationCache.hasCompiledScript(id)) return true;
        if (requireLookup != null && requireLookup.exists(id)) return true;
        return false;
    }

    public String resolve(JSObject parent, String id) {
        String parentFilename = parent == null ? "/." : (String)parent.getMember("filename");
        String cachedResolution = scriptEngineAndCompilationCache.getCachedModuleResolution(parentFilename, id);
        if (cachedResolution != null) return cachedResolution;
        String res = resolveUncached(parent, parentFilename, id);
        if (res == null) return null;
        scriptEngineAndCompilationCache.cacheModuleResolution(parentFilename, id, res);
        return res;
    }

    private String resolveUncached(JSObject parent, String parentFilename, String id) {
        String resolvedCoreModule = resolveFile(id);
        if (resolvedCoreModule != null) return resolvedCoreModule;
        try {
            Path parentFilepath = Paths.get(parentFilename);
            if (id.startsWith("./") || id.startsWith("../") || id.startsWith("/")) {
                String absoluteId = parentFilepath.resolveSibling(id).normalize().toString();
                String resolvedFile = resolveFile(absoluteId);
                if (resolvedFile != null) return resolvedFile;
                resolvedFile = resolveDirectory(parent, absoluteId);
                if (resolvedFile != null) return resolvedFile;
            } else {
                while (true) {
                    parentFilepath = parentFilepath.getParent();
                    if (parentFilepath == null) break;
                    if (parentFilepath.getFileName() != null && parentFilepath.getFileName().toString().equals("node_modules")) continue;
                    String absoluteId = parentFilepath.resolve("node_modules").resolve(id).normalize().toString();
                    String resolvedFile = resolveFile(absoluteId);
                    if (resolvedFile != null) return resolvedFile;
                    resolvedFile = resolveDirectory(parent, absoluteId);
                    if (resolvedFile != null) return resolvedFile;
                }
            }
            return null;
        } catch (NashornException e) {
            throw e;
        } catch (RuntimeException e) {
            throw scriptEngineAndCompilationCache.reasonToException(scriptContext, e);
        }
    }

    private String resolveFile(String absoluteId) {
        if (moduleExists(absoluteId)) return absoluteId;
        if (moduleExists(absoluteId + ".js")) return absoluteId + ".js";
        if (moduleExists(absoluteId + ".json")) return absoluteId + ".json";
        return null;
    }

    private String resolveDirectory(JSObject parent, String absoluteId) {
        String main = getMainFromPackageJson(parent, absoluteId);
        if (main != null) {
            String resolvedFile = resolveFile(Paths.get(absoluteId).resolve(main).normalize().toString());
            if (resolvedFile != null) return resolvedFile;
        } else {
            if (moduleExists(absoluteId + "/index.js")) {
                return absoluteId + "/index.js";
            }
            if (moduleExists(absoluteId + "/index.json")) {
                return absoluteId + "/index.json";
            }
        }
        return null;
    }

    private String getMainFromPackageJson(JSObject parent, String absoluteId) {
        JSObject packageJsonModule = getModule(parent, absoluteId + "/package.json");
        if (packageJsonModule != null) {
            Object exports = packageJsonModule.getMember("exports");
            if (exports instanceof JSObject) {
                Object mainObj = ((JSObject) exports).getMember("main");
                if (mainObj instanceof String) {
                    return (String) mainObj;
                }
            }
        }
        return null;
    }

    private JSObject getModule(JSObject parent, String filename) {
        {
            Object module = moduleCache.getMember(filename);
            // could be instanceof Undefined instead, rather than null
            if (module instanceof JSObject) return (JSObject)module;
        }
        {
            CompiledScript compiledScript = scriptEngineAndCompilationCache.getCompiledScript(filename);
            if (compiledScript != null) {
                return getModuleFromCompiledScript(parent, filename, compiledScript);
            }
        }
        {
            try (Reader moduleContent = requireLookup == null ? null : requireLookup.getContent(filename)) {
                if (moduleContent != null) {
                    return requireModuleFromReader(parent, filename, moduleContent);
                }
            } catch (IOException e) {
                throw scriptEngineAndCompilationCache.reasonToException(scriptContext, e);
            }
        }
        return null;
    }

    public JSObject requireModuleFromReader(JSObject parent, String filename, Reader moduleContent) {
        try {
            if (filename.endsWith(".json")) {
                String moduleContentString = readFully(moduleContent);
                return getModuleFromJson(parent, filename, moduleContentString);
            } else {
                CompiledScript compiledScript;
                try (Reader wrappedModule = new SequenceReader(new StringReader(MODULE_PREFIX), moduleContent, new StringReader(MODULE_SUFFIX))) {
                    compiledScript = scriptEngineAndCompilationCache.compileAndCache(filename, wrappedModule);
                } catch (ScriptException e) {
                    Throwable cause = e.getCause();
                    if (cause instanceof NashornException) throw (NashornException)cause;
                    if (cause instanceof RuntimeException) throw (RuntimeException)cause;
                    throw e;
                }
                return getModuleFromCompiledScript(parent, filename, compiledScript);
            }
        } catch (NashornException e) {
            throw e;
        } catch (ScriptException | IOException | RuntimeException e) {
            throw scriptEngineAndCompilationCache.reasonToException(scriptContext, e);
        }
    }

    // intended for testing
    public JSObject requireModuleFromReaderInGlobalContext(JSObject parent, String filename, Reader moduleContent) {
        try {
            if (filename.endsWith(".json")) {
                String moduleContentString = readFully(moduleContent);
                return getModuleFromJson(parent, filename, moduleContentString);
            } else {
                JSObject module = newModule(parent, filename);
                moduleCache.setMember(filename, module);
                scriptContext.setAttribute("exports", module.getMember("exports"), ScriptContext.ENGINE_SCOPE);
                scriptContext.setAttribute("require", module.getMember("require"), ScriptContext.ENGINE_SCOPE);
                scriptContext.setAttribute("module", module, ScriptContext.ENGINE_SCOPE);
                scriptContext.setAttribute("__filename", filename, ScriptContext.ENGINE_SCOPE);
                scriptContext.setAttribute("__dirname", dirname(filename), ScriptContext.ENGINE_SCOPE);
                try {
                    scriptEngineAndCompilationCache.getScriptEngine().eval(moduleContent, scriptContext);
                } catch (ScriptException | RuntimeException e) {
                    moduleCache.removeMember(filename);
                    throw e;
                }
                module.setMember("loaded", Boolean.TRUE);
                return module;
            }
        } catch (NashornException e) {
            throw e;
        } catch (ScriptException e) {
            Throwable cause = e.getCause();
            if (cause instanceof NashornException) throw (NashornException)cause;
            if (cause instanceof RuntimeException) throw (RuntimeException)cause;
            throw scriptEngineAndCompilationCache.reasonToException(scriptContext, e);
        } catch (IOException | RuntimeException e) {
            throw scriptEngineAndCompilationCache.reasonToException(scriptContext, e);
        }
    }

    public Object requireFromReader(JSObject parent, String filename, Reader reader) {
        JSObject module = requireModuleFromReader(parent, filename, reader);
        if (module == null) return null;
        return module.getMember("exports");
    }

    public Object requireFromReaderInGlobalContext(JSObject parent, String filename, Reader reader) {
        JSObject module = requireModuleFromReaderInGlobalContext(parent, filename, reader);
        if (module == null) return null;
        return module.getMember("exports");
    }

    private JSObject getModuleFromJson(JSObject parent, String filename, String moduleContentString) {
        JSObject module = newModule(parent, filename);
        module.setMember("exports", scriptEngineAndCompilationCache.jsonParse(scriptContext, moduleContentString));
        // since it can't be recursive, ok to cache after parsing
        moduleCache.setMember(filename, module);
        module.setMember("loaded", Boolean.TRUE);
        return module;
    }

    private JSObject getModuleFromCompiledScript(JSObject parent, String filename, CompiledScript compiledScript) {
        try {
            JSObject module = newModule(parent, filename);
            moduleCache.setMember(filename, module);
            try {
                JSObject moduleWrapper = (JSObject)(compiledScript.eval(scriptContext));
                moduleWrapper.call(module.getMember("exports"), module.getMember("exports"), module.getMember("require"), module, filename, dirname(filename));
            } catch (ScriptException | RuntimeException e) {
                moduleCache.removeMember(filename);
                throw e;
            }
            module.setMember("loaded", Boolean.TRUE);
            return module;
        } catch (ScriptException e) {
            Throwable cause = e.getCause();
            if (cause instanceof NashornException) throw (NashornException)cause;
            if (cause instanceof RuntimeException) throw (RuntimeException)cause;
            throw new RuntimeException(e);
       }
    }

    private JSObject newModule(JSObject parent, String filename) {
        return scriptEngineAndCompilationCache.newModule(scriptContext, parent, filename,
            (thisModule, newId) -> requireModule(thisModule, newId),
            (thisModule, newId) -> resolve(thisModule, newId));
    }

//    private JSObject require(JSObject main, JSObject parent, String parentFilename, String id) throws ScriptException {
//        String filename = resolve(main, parent, parentFilename, id);
//        if (filename == null) return null;
//        JSObject module = getModule(main, parent, filename);
//        if (module == null) return null;
//        return (JSObject)module.getMember("exports");
//    }

    public Object require(JSObject parent, String id) {
        JSObject module = requireModule(parent, id);
        if (module == null) return null;
        return module.getMember("exports");
    }

    public JSObject requireModule(JSObject parent, String id) {
        String parentFilename = parent == null ? "/." : (String)parent.getMember("filename");
        String cachedResolution = scriptEngineAndCompilationCache.getCachedModuleResolution(parentFilename, id);
        if (cachedResolution != null) {
            return requireModuleFile(parent, cachedResolution);
        }
        JSObject module = requireModuleUncached(parent, parentFilename, id);
        if (module == null) return null;
        String filename = (String) module.getMember("filename");
        scriptEngineAndCompilationCache.cacheModuleResolution(parentFilename, id, filename);
        return module;
    }

    private JSObject requireModuleUncached(JSObject parent, String parentFilename, String id) {
        JSObject resolvedCoreModule = requireModuleFile(parent, id);
        if (resolvedCoreModule != null) return resolvedCoreModule;
        try {
            Path parentFilepath = Paths.get(parentFilename);
            if (id.startsWith("./") || id.startsWith("../") || id.startsWith("/")) {
                String absoluteId = parentFilepath.resolveSibling(id).normalize().toString();
                JSObject resolvedModule = requireModuleFile(parent, absoluteId);
                if (resolvedModule != null) return resolvedModule;
                resolvedModule = requireModuleDirectory(parent, absoluteId);
                if (resolvedModule != null) return resolvedModule;
            } else {
                while (true) {
                    parentFilepath = parentFilepath.getParent();
                    if (parentFilepath == null) break;
                    if (parentFilepath.getFileName() != null && parentFilepath.getFileName().toString().equals("node_modules")) continue;
                    String absoluteId = parentFilepath.resolve("node_modules").resolve(id).normalize().toString();
                    JSObject resolvedModule = requireModuleFile(parent, absoluteId);
                    if (resolvedModule != null) return resolvedModule;
                    resolvedModule = requireModuleDirectory(parent, absoluteId);
                    if (resolvedModule != null) return resolvedModule;
                }
            }
            return null;
        } catch (NashornException e) {
            throw e;
        } catch (Exception e) {
            throw scriptEngineAndCompilationCache.reasonToException(scriptContext, e);
        }
    }

    private JSObject requireModuleFile(JSObject parent, String absoluteId) {
        JSObject module = getModule(parent, absoluteId);
        if (module != null) return module;
        module = getModule(parent, absoluteId + ".js");
        if (module != null) return module;
        module = getModule(parent, absoluteId + ".json");
        if (module != null) return module;
        return null;
    }

    private JSObject requireModuleDirectory(JSObject parent, String absoluteId) {
        String main = getMainFromPackageJson(parent, absoluteId);
        if (main != null) {
            JSObject resolvedModule = requireModuleFile(parent, Paths.get(absoluteId).resolve(main).normalize().toString());
            if (resolvedModule != null) return resolvedModule;
        } else {
            JSObject module = getModule(parent, absoluteId + "/index.js");
            if (module != null) return module;
            module = getModule(parent, absoluteId + "/index.json");
            if (module != null) return module;
        }
        return null;
    }

}
