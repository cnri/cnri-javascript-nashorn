package net.cnri.util.javascript.nashorn;

import java.io.Reader;
import java.io.StringReader;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import javax.script.CompiledScript;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import jdk.nashorn.api.scripting.JSObject;
import jdk.nashorn.api.scripting.NashornException;

public class JavaScriptRunner {
    private final ScriptEngineAndCompilationCache scriptEngineAndCompilationCache;
    private final ScriptEngine scriptEngine;
    private final ScriptContext scriptContext;
    private final JavaScriptEventLoop eventLoop;
    private final ModuleSystem moduleSystem;

    public JavaScriptRunner(ScriptEngineAndCompilationCache scriptEngineAndCompilationCache, ScriptContext scriptContext, JavaScriptEventLoop eventLoop, RequireLookup requireLookup, Thread.UncaughtExceptionHandler uncaughtExceptionHandler, Object logger) {
        this.scriptEngineAndCompilationCache = scriptEngineAndCompilationCache;
        this.scriptEngine = scriptEngineAndCompilationCache.getScriptEngine();
        this.scriptContext = scriptContext != null ? scriptContext : scriptEngine.getContext();
        this.eventLoop = eventLoop;
        JSObject cnriJse = (JSObject)((JSObject)this.scriptContext.getAttribute("cnri")).getMember("_jse");
        setCnriJseGlobals(cnriJse, uncaughtExceptionHandler, logger);
        JSObject moduleCache = (JSObject)cnriJse.getMember("moduleCache");
        this.moduleSystem = new ModuleSystem(scriptEngineAndCompilationCache, scriptContext, moduleCache, requireLookup);
    }

    public ScriptContext getScriptContext() {
        return scriptContext;
    }

    public JavaScriptEventLoop getEventLoop() {
        return eventLoop;
    }

    private void setCnriJseGlobals(JSObject cnriJse, Thread.UncaughtExceptionHandler uncaughtExceptionHandler, Object logger) {
        cnriJse.setMember("logger", logger);
        if (uncaughtExceptionHandler == null) {
            uncaughtExceptionHandler = (t, e) -> scriptEngineAndCompilationCache.loggerWarn(scriptContext, t, e);
        }
        cnriJse.setMember("uncaughtExceptionHandler", uncaughtExceptionHandler);
    }

    public Object jsonParse(String json) throws ScriptException {
        try {
            return scriptEngineAndCompilationCache.jsonParse(scriptContext, json);
        } catch (NashornException ne) {
            ScriptException se = toScriptException(ne);
            throw se;
        }
    }

    public String jsonStringify(Object obj) throws ScriptException {
        try {
            return scriptEngineAndCompilationCache.jsonStringify(scriptContext, obj);
        } catch (NashornException ne) {
            ScriptException se = toScriptException(ne);
            throw se;
        }
    }

    public Object evalCompiled(String name) throws InterruptedException, ScriptException {
        CompiledScript compiledScript = scriptEngineAndCompilationCache.getCompiledScript(name);
        if (compiledScript == null) throw new ScriptException("No such compiled script " + name);
        try {
            return eventLoop.submit(() -> compiledScript.eval(scriptContext)).get();
        } catch (ExecutionException e) {
            throw rethrowAsScriptException(e);
        }
    }

    public Object eval(String source) throws InterruptedException, ScriptException {
        try {
            return eventLoop.submit(() -> scriptEngine.eval(source, scriptContext)).get();
        } catch (ExecutionException e) {
            throw rethrowAsScriptException(e);
        }
    }

    public Object eval(Reader source) throws InterruptedException, ScriptException {
        try {
            return eventLoop.submit(() -> scriptEngine.eval(source, scriptContext)).get();
        } catch (ExecutionException e) {
            throw rethrowAsScriptException(e);
        }
    }

    public Object requireById(String id) throws InterruptedException, ScriptException {
        try {
            return eventLoop.submit(() -> moduleSystem.require(null, id)).get();
        } catch (ExecutionException e) {
            throw rethrowAsScriptException(e);
        }
    }

    public <T> T submitAndGet(Callable<T> callable) throws InterruptedException, ScriptException {
        try {
            return eventLoop.submit(callable).get();
        } catch (ExecutionException e) {
            throw rethrowAsScriptException(e);
        }
    }

    public Object requireFromContent(String content) throws InterruptedException, ScriptException {
        return requireFromReader(new StringReader(content));
    }

    public Object requireFromReader(Reader reader) throws InterruptedException, ScriptException {
        try {
            return eventLoop.submit(() -> moduleSystem.requireFromReader(null, "/.", reader)).get();
        } catch (ExecutionException e) {
            throw rethrowAsScriptException(e);
        }
    }

    public Object requireFromReaderInGlobalContext(Reader reader) throws InterruptedException, ScriptException {
        try {
            return eventLoop.submit(() -> moduleSystem.requireFromReaderInGlobalContext(null, "/.", reader)).get();
        } catch (ExecutionException e) {
            throw rethrowAsScriptException(e);
        }
    }

    public Object invokeFunction(String name, Object... args) throws NoSuchMethodException, InterruptedException, ScriptException {
        Object obj = scriptContext.getAttribute(name);
        if (!(obj instanceof JSObject)) throw new NoSuchMethodException(name);
        JSObject fn = (JSObject)obj;
        try {
            return eventLoop.submit(() -> fn.call(null, args)).get();
        } catch (ExecutionException e) {
            throw rethrowAsScriptException(e);
        }
    }

    public Object awaitPromise(Object maybePromise) throws InterruptedException, ScriptException {
        if (!(maybePromise instanceof JSObject)) return maybePromise;
        JSObject maybePromiseJSObject = (JSObject)maybePromise;
        if (!(maybePromiseJSObject.hasMember("then"))) return maybePromise;
        Object then = maybePromiseJSObject.getMember("then");
        if (!(then instanceof JSObject)) return maybePromise;
        JSObject thenJSObject = (JSObject)then;
        if (!thenJSObject.isFunction()) return maybePromise;
        CompletableFuture<Object> javaPromise = new CompletableFuture<>();
        try {
            eventLoop.submit(() ->
                thenJSObject.call(maybePromise,
                    (Consumer<Object>)(res -> javaPromise.complete(res)),
                    (Consumer<Object>)(reason -> javaPromise.completeExceptionally(scriptEngineAndCompilationCache.reasonToException(scriptContext, reason)))));
            return javaPromise.get();
        } catch (ExecutionException e) {
            throw rethrowAsScriptException(e);
        }
    }

    private RuntimeException rethrowAsScriptException(ExecutionException e) throws ScriptException, InterruptedException {
        Throwable cause = e.getCause();
        while (cause instanceof ExecutionException || cause instanceof CompletionException) {
            cause = cause.getCause();
        }
        if (cause instanceof NashornException) {
            NashornException ne = (NashornException)cause;
            ScriptException se = toScriptException(ne);
            throw se;
        }
        if (cause instanceof ScriptException) throw (ScriptException)cause;
        if (cause instanceof InterruptedException) throw (InterruptedException)cause;
        if (cause instanceof RuntimeException) throw (RuntimeException)cause;
        if (cause instanceof Error) throw (Error)cause;
        if (cause instanceof Exception) throw new ScriptException((Exception)cause);
        throw new ScriptException(e);
    }

    private ScriptException toScriptException(NashornException ne) {
        String message = ne.getMessage();
        if (ne.getEcmaError() instanceof JSObject) {
            JSObject jsError = (JSObject)ne.getEcmaError();
            if (jsError.hasMember("name")) message += ": " + jsError.getMember("name");
            if (jsError.hasMember("message")) message += ": " + jsError.getMember("message");
        }
        ScriptException se = new ScriptException(message, ne.getFileName(), ne.getLineNumber(), ne.getColumnNumber());
        se.initCause(ne);
        return se;
    }

}
