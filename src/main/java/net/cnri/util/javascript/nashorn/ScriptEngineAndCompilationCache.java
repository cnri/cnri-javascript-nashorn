package net.cnri.util.javascript.nashorn;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import jdk.nashorn.api.scripting.JSObject;
import jdk.nashorn.api.scripting.NashornException;
import jdk.nashorn.api.scripting.ScriptObjectMirror;

public class ScriptEngineAndCompilationCache {
    private static final String BUILTIN_COMPILED_SCRIPT_PREFIX = "cnri/_jse/";
    private final ScriptEngine scriptEngine;
    private final ConcurrentMap<String, CompiledScript> compilationCache;
    private final Map<String, Object> extraGlobals = new ConcurrentHashMap<>();
    private final Map<String, Map<String, String>> moduleResolveCache = new ConcurrentHashMap<>();

    public ScriptEngineAndCompilationCache(ScriptEngine scriptEngine) {
        this.scriptEngine = scriptEngine;
        this.compilationCache = new ConcurrentHashMap<>();
        initialize();
    }

    private void initialize() {
        Object originalFilename = scriptEngine.get(ScriptEngine.FILENAME);
        try {
            initCompileResource("initialize.js");
            initCompileString("thrower.js", "function (reason) { throw reason; }");
            initCompileString("json.js", "JSON");
            initCompileResource("module.js");
            initCompileResource("logger-warn.js");
            initCompileResource("console.js");
            initCompileResource("timers.js");
            initCompileResource("process.js");
            initCompileResource("promise.js");
            initCompileResource("cnri-javascript-core-js-bundle.min.js");
        } catch (IOException | ScriptException e) {
            throw new AssertionError(e);
        } finally {
            scriptEngine.put(ScriptEngine.FILENAME, originalFilename);
        }
    }

    private void initCompileString(String name, String script) throws ScriptException {
        scriptEngine.put(ScriptEngine.FILENAME, name);
        compilationCache.putIfAbsent(BUILTIN_COMPILED_SCRIPT_PREFIX + name, ((Compilable)scriptEngine).compile(script));
    }

    public void put(String key, Object obj) {
        extraGlobals.put(key, obj);
    }

    private void initCompileResource(String name) throws ScriptException, IOException {
        try (InputStreamReader isr = new InputStreamReader(JavaScriptRunner.class.getResourceAsStream(name), StandardCharsets.UTF_8)) {
            scriptEngine.put(ScriptEngine.FILENAME, name);
            compilationCache.putIfAbsent(BUILTIN_COMPILED_SCRIPT_PREFIX + name, ((Compilable)scriptEngine).compile(isr));
        }
    }

    public synchronized void clearCache() {
        moduleResolveCache.clear();
        Iterator<String> iter = compilationCache.keySet().iterator();
        while (iter.hasNext()) {
            if (!iter.next().startsWith(BUILTIN_COMPILED_SCRIPT_PREFIX)) {
                iter.remove();
            }
        }
    }

    public synchronized CompiledScript compileAndCache(String name, String script) throws ScriptException {
        Object originalFilename = scriptEngine.get(ScriptEngine.FILENAME);
        try {
            scriptEngine.put(ScriptEngine.FILENAME, name);
            CompiledScript compiledScript = ((Compilable)scriptEngine).compile(script);
            compilationCache.putIfAbsent(name, compiledScript);
            return compiledScript;
        } finally {
            scriptEngine.put(ScriptEngine.FILENAME, originalFilename);
        }
    }

    public synchronized CompiledScript compileAndCache(String name, Reader script) throws ScriptException {
        Object originalFilename = scriptEngine.get(ScriptEngine.FILENAME);
        try {
            scriptEngine.put(ScriptEngine.FILENAME, name);
            CompiledScript compiledScript = ((Compilable)scriptEngine).compile(script);
            compilationCache.putIfAbsent(name, compiledScript);
            return compiledScript;
        } finally {
            scriptEngine.put(ScriptEngine.FILENAME, originalFilename);
        }
    }

    public ScriptEngine getScriptEngine() {
        return scriptEngine;
    }

    public CompiledScript getCompiledScript(String name) {
        return compilationCache.get(name);
    }

    public boolean hasCompiledScript(String name) {
        return compilationCache.containsKey(name);
    }

    public void initializeScriptContext(ScriptContext scriptContext) {
        try {
            scriptContext = scriptContext != null ? scriptContext : scriptEngine.getContext();
            compilationCache.get(BUILTIN_COMPILED_SCRIPT_PREFIX + "initialize.js").eval(scriptContext);
            JSObject cnriJse = (JSObject)((JSObject)scriptContext.getAttribute("cnri")).getMember("_jse");
            cnriJse.setMember("threadLocalJavaScriptEventLoop", JavaScriptEventLoop.threadLocalJavaScriptEventLoop);
            compilationCache.get(BUILTIN_COMPILED_SCRIPT_PREFIX + "console.js").eval(scriptContext);
            compilationCache.get(BUILTIN_COMPILED_SCRIPT_PREFIX + "timers.js").eval(scriptContext);
            compilationCache.get(BUILTIN_COMPILED_SCRIPT_PREFIX + "process.js").eval(scriptContext);
            compilationCache.get(BUILTIN_COMPILED_SCRIPT_PREFIX + "promise.js").eval(scriptContext);
            compilationCache.get(BUILTIN_COMPILED_SCRIPT_PREFIX + "cnri-javascript-core-js-bundle.min.js").eval(scriptContext);
            for (Map.Entry<String, Object> entry : extraGlobals.entrySet()) {
                scriptContext.setAttribute(entry.getKey(), entry.getValue(), ScriptContext.ENGINE_SCOPE);
            }
        } catch (ScriptException e) {
            throw new AssertionError(e);
        }
    }

    public String getCachedModuleResolution(String parentFilename, String id) {
        if (parentFilename == null) parentFilename = "/.";
        // for absolute ids, resolution is not specific to parent
        if (id.startsWith("./") || id.startsWith("../") || id.startsWith("/")) {
            id = Paths.get(parentFilename).resolveSibling(id).normalize().toString();
            parentFilename = "/.";
        }
        String key = getKeyFromParent(parentFilename);
        Map<String, String> subMap = moduleResolveCache.get(key);
        if (subMap == null) return null;
        return subMap.get(id);
    }

    public void cacheModuleResolution(String parentFilename, String id, String resolution) {
        if (resolution == null) return;
        if (parentFilename == null) parentFilename = "/.";
        // for absolute ids, resolution is not specific to parent
        if (id.startsWith("./") || id.startsWith("../") || id.startsWith("/")) {
            id = Paths.get(parentFilename).resolveSibling(id).normalize().toString();
            parentFilename = "/.";
        }
        String key = getKeyFromParent(parentFilename);
        Map<String, String> subMap = moduleResolveCache.computeIfAbsent(key, s -> new ConcurrentHashMap<>());
        subMap.put(id, resolution);
    }

    private String getKeyFromParent(String parentFilename) {
        // otherwise, only the container matters
        java.nio.file.Path grandparentPath = Paths.get(parentFilename).getParent();
        if (grandparentPath == null) return "/";
        else return grandparentPath.normalize().toString();
    }

    @FunctionalInterface
    public static interface ResolveFunction {
        String resolve(JSObject module, String id);
    }

    @FunctionalInterface
    public static interface RequireModuleFunction {
        JSObject requireModule(JSObject module, String id);
    }

    public JSObject newModule(ScriptContext scriptContext, JSObject parent, String filename, RequireModuleFunction requireModule, ResolveFunction resolve) {
        try {
            JSObject newModuleObjectBuilder = (JSObject)(getCompiledScript(BUILTIN_COMPILED_SCRIPT_PREFIX + "module.js").eval(scriptContext));
            return (JSObject)newModuleObjectBuilder.call(null, parent, filename, requireModule, resolve);
        } catch (ScriptException e) {
            throw new AssertionError(e);
        }
    }

    public Object jsonParse(ScriptContext scriptContext, String json) {
        try {
            return ((ScriptObjectMirror)getCompiledScript(BUILTIN_COMPILED_SCRIPT_PREFIX + "json.js").eval(scriptContext)).callMember("parse", json);
        } catch (ScriptException e) {
            throw new AssertionError(e);
        }
    }

    public String jsonStringify(ScriptContext scriptContext, Object obj) {
        try {
            Object res = ((ScriptObjectMirror)getCompiledScript(BUILTIN_COMPILED_SCRIPT_PREFIX + "json.js").eval(scriptContext)).callMember("stringify", obj);
            if (ScriptObjectMirror.isUndefined(res)) return null;
            return (String)res;
        } catch (ScriptException e) {
            throw new AssertionError(e);
        }
    }

    public void loggerWarn(ScriptContext scriptContext, Thread thread, Throwable exception) {
        try {
            JSObject loggerWarn = (JSObject)getCompiledScript(BUILTIN_COMPILED_SCRIPT_PREFIX + "logger-warn.js").eval(scriptContext);
            loggerWarn.call(null, thread, exception);
        } catch (ScriptException e) {
            throw new AssertionError(e);
        }
    }

    public NashornException reasonToException(ScriptContext scriptContext, Object reason) {
        try {
            JSObject thrower = (JSObject)(getCompiledScript(BUILTIN_COMPILED_SCRIPT_PREFIX + "thrower.js").eval(scriptContext));
            thrower.call(null, reason);
            return null;
        } catch (ScriptException e) {
            throw new AssertionError(e);
        } catch (NashornException ne) {
            if (reason instanceof Throwable) {
                if (ne == reason) return ne;
                if (ne.getCause() == null) {
                    try {
                        ne.initCause((Throwable)reason);
                    } catch (Exception ex) {
                        ne.addSuppressed((Throwable)reason);
                    }
                } else {
                    ne.addSuppressed((Throwable)reason);
                }
            }
            return ne;
        }
    }
}
