package net.cnri.util.javascript.nashorn;
import java.util.concurrent.atomic.AtomicLong;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.SimpleScriptContext;

import jdk.nashorn.api.scripting.JSObject;

/**
 * A provider of {@link JavaScriptRunner} objects.  This implementation uses a single pool of global objects
 * which are re-used, so assumes that non-concurrent pollution of the global environment is not an issue.
 */
public class JavaScriptEnvironment {
    private final RequireLookup requireLookup;
    private final ScriptEngineAndCompilationCache scriptEngineAndCompilationCache;
    private final BoundedObjectPool<JavaScriptEventLoop> eventLoopPool;
    private final BoundedObjectPool<ScriptContext> globalPool;
    private final AtomicLong generation = new AtomicLong(1);

    public JavaScriptEnvironment(RequireLookup requireLookup) {
        this(requireLookup, JavaScriptEnvironment.class.getClassLoader());
    }

    public JavaScriptEnvironment(RequireLookup requireLookup, ClassLoader classLoader) {
        this.requireLookup = requireLookup;
        setNashornArgs();
        ScriptEngineManager manager = new ScriptEngineManager();
        ClassLoader contextClassLoader = null;
        if (classLoader != null) {
            contextClassLoader = Thread.currentThread().getContextClassLoader();
            Thread.currentThread().setContextClassLoader(classLoader);
        }
        ScriptEngine scriptEngine = manager.getEngineByName("nashorn");
        if (classLoader != null) {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }
        this.scriptEngineAndCompilationCache = new ScriptEngineAndCompilationCache(scriptEngine);
        this.eventLoopPool = new BoundedObjectPool<JavaScriptEventLoop>() {
            @Override
            protected JavaScriptEventLoop create() {
                return new JavaScriptEventLoop();
            }

            @Override
            protected boolean reset(JavaScriptEventLoop obj) {
                try {
                    obj.clear();
                    return true;
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    return false;
                }
            }

            @Override
            protected void destroy(JavaScriptEventLoop obj) {
                obj.shutdown();
            }
        };
        this.globalPool = new BoundedObjectPool<ScriptContext>() {
            @Override
            protected ScriptContext create() {
                ScriptContext scriptContext = new SimpleScriptContext();
                scriptContext.setBindings(scriptEngineAndCompilationCache.getScriptEngine().createBindings(), ScriptContext.ENGINE_SCOPE);
                scriptEngineAndCompilationCache.initializeScriptContext(scriptContext);
                JSObject cnriJse = (JSObject)((JSObject)scriptContext.getAttribute("cnri")).getMember("_jse");
                cnriJse.setMember("generation", generation.get());
                return scriptContext;
            }

            @Override
            protected boolean reset(ScriptContext obj) {
                JSObject cnriJse = (JSObject)((JSObject)obj.getAttribute("cnri")).getMember("_jse");
                long objGeneration = ((Number)cnriJse.getMember("generation")).longValue();
                if (objGeneration < generation.get()) return false;
                else return true;
            }

            @Override
            protected void destroy(ScriptContext obj) {
                // no-op
            }
        };
    }

    private void setNashornArgs() {
        if (System.getProperty("nashorn.args") != null) return;
        String version = System.getProperty("java.version");
        if (!version.startsWith("1.") && !version.startsWith("10") && version.startsWith("1") && System.getProperty("nashorn.args") == null) {
            System.setProperty("nashorn.args", "--language=es6 --no-deprecation-warning");
        } else {
            System.setProperty("nashorn.args", "--language=es6");
        }
    }

    public ScriptEngineAndCompilationCache getScriptEngineAndCompilationCache() {
        return scriptEngineAndCompilationCache;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        globalPool.setMaxPoolSize(maxPoolSize);
        eventLoopPool.setMaxPoolSize(maxPoolSize);
    }

    /**
     * Clears the cache of compiled scripts, as well as the global pool to eliminate each module cache.
     */
    public void clearCache() {
        scriptEngineAndCompilationCache.clearCache();
        generation.incrementAndGet();
        globalPool.clear();
    }

    public JavaScriptRunner getRunner(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, Object logger) {
        ScriptContext scriptContext = globalPool.obtain();
        JavaScriptEventLoop eventLoop = eventLoopPool.obtain();
        return new JavaScriptRunner(scriptEngineAndCompilationCache, scriptContext, eventLoop, requireLookup, uncaughtExceptionHandler, logger);
    }

    public void recycle(JavaScriptRunner runner) {
        globalPool.recycle(runner.getScriptContext());
        eventLoopPool.recycle(runner.getEventLoop());
    }

    public void recycleDiscardingScriptContext(JavaScriptRunner runner) {
        eventLoopPool.recycle(runner.getEventLoop());
    }

    public void warmUp() {
        recycle(getRunner(null, null));
    }

    public void shutdown() {
        setMaxPoolSize(0);
        eventLoopPool.clear();
    }
}
