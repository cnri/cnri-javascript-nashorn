package net.cnri.util.javascript.nashorn;
import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;

public class SequenceReader extends Reader {
    private final Reader[] readers;
    int index;

    public SequenceReader(Reader... readers) {
        this.readers = readers;
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        if (index >= readers.length) return -1;
        for ( ; index < readers.length; index++) {
            int read = readers[index].read(cbuf, off, len);
            if (read < 0) {
                readers[index].close();
                continue;
            } else {
                return read;
            }
        }
        return -1;
    }

    @Override
    public int read(CharBuffer target) throws IOException {
        if (index >= readers.length) return -1;
        for ( ; index < readers.length; index++) {
            int read = readers[index].read(target);
            if (read < 0) {
                readers[index].close();
                continue;
            } else {
                return read;
            }
        }
        return -1;
    }

    @Override
    public int read() throws IOException {
        if (index >= readers.length) return -1;
        for ( ; index < readers.length; index++) {
            int read = readers[index].read();
            if (read < 0) {
                readers[index].close();
                continue;
            } else {
                return read;
            }
        }
        return -1;
    }

    @Override
    public int read(char[] cbuf) throws IOException {
        if (index >= readers.length) return -1;
        for ( ; index < readers.length; index++) {
            int read = readers[index].read(cbuf);
            if (read < 0) {
                readers[index].close();
                continue;
            } else {
                return read;
            }
        }
        return -1;
    }

    @Override
    public void close() throws IOException {
        IOException exception = null;
        for ( ; index < readers.length; index++) {
            try {
                readers[index].close();
            } catch (IOException e) {
                if (exception == null) {
                    exception = e;
                } else {
                    exception.addSuppressed(e);
                }
            }
        }
        if (exception != null) throw exception;
    }

}
