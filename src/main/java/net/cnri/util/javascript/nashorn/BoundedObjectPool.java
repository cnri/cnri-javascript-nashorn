package net.cnri.util.javascript.nashorn;


import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

/** Pool of objects bounded by size where threads may create new objects whenever the pool is empty.
 * Use by subclassing and defining {@link #create()}, {@link #reset(Object)}, and {@link #destroy(Object)}.
 */
public abstract class BoundedObjectPool<T> {
    private ConcurrentLinkedQueue<T> pool = new ConcurrentLinkedQueue<>();
    private AtomicInteger poolSize = new AtomicInteger();
    private int maxPoolSize = 100;

    /**
     * Returns the maximum pool size, 100 by default.
     * @return the maximum pool size, 100 by default.
     */
    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    /**
     * Sets the maximum pool size.
     * @param maxPoolSize the new maximum pool size
     */
    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    /**
     * Creates a new object if the pool is empty.  Implemented by subclasses.
     * @return a new object.
     */
    protected abstract T create();

    /**
     * Called on an object before it is returned to the pool.  Implemented by subclasses.
     * @param obj an object to be reset before returning to the pool.
     * @return whether the object can be returned; if false {@link #destroy(Object)} will be called.
     */
    protected abstract boolean reset(T obj);

    /**
     * Called on an object which will not be returned to the pool.  Implemented by subclasses.
     * @param obj an object which will not be returned to the pool.
     */
    protected abstract void destroy(T obj);

    /**
     * Removes all objects from the pool, calling {@link #destroy(Object)} on each.
     */
    public void clear() {
        T obj;
        while ((obj = pool.poll()) != null) {
            poolSize.decrementAndGet();
            destroy(obj);
        }
    }

    /**
     * Obtains a new object from the pool.  Calls {@link #create()} to create a new object if the pool is empty.
     * @return a new object from the pool, or else a newly-created object.
     */
    public T obtain() {
        T res = pool.poll();
        if (res!=null) {
            poolSize.decrementAndGet();
            return res;
        } else {
            return create();
        }
    }

    /**
     * Returns an object to the pool.  Calls {@link #reset(Object)} before returning the object.
     * If returning the object might cause the pool size to exceed its maximum,
     * the object is not returned to the pool; {@link #reset(Object)} is not called,
     * and {@link #destroy(Object)} is called.
     *
     * @param obj an object to return to the pool
     */
    public void recycle(T obj) {
        if (poolSize.incrementAndGet() < maxPoolSize && reset(obj)) {
            pool.offer(obj);
        } else {
            poolSize.decrementAndGet();
            destroy(obj);
        }
    }

}
