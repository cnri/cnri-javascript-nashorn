package net.cnri.util.javascript.nashorn;
import java.io.Reader;

public interface RequireLookup {
    /**
     * Returns true if the "file" exists and is a file (not a directory), otherwise false.
     * @param filename name of the file
     * @return true if the "file" exists and is a file (not a directory), otherwise false.
     */
    boolean exists(String filename);

    /**
     * Returns a {@code Reader} with the content of the file if it exists, otherwise null.
     * @param filename name of the file
     * @return a {@code Reader} with the content of the file if it exists, otherwise null.
     */
    Reader getContent(String filename);
}
