function (t, e) { 
    var message = 'Uncaught exception';
    if (t) message += ' on ' + t.getName();
    if (e instanceof Packages.jdk.nashorn.api.scripting.NashornException) {
        var ee = e.getEcmaError();
        if (ee) {
            if (ee.name) message += ': ' + ee.name;
            if (ee.message) message += ': ' + ee.message;
            if (ee.cause) {
                if (ee.cause.name) message += ': ' + ee.cause.name;
                if (ee.cause.message) message += ': ' + ee.cause.message;
            }
        }
    }
    if (typeof cnri !== 'undefined' && cnri._jse && cnri._jse.logger && typeof cnri._jse.logger.warn === 'function') {
        cnri._jse.logger.warn(message, e); 
    } else {
        java.lang.System.err.println(message);
        e.printStackTrace();
    }
}