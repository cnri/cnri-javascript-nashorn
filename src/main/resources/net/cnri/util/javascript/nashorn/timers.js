//# sourceURL=timers.js
(function (global) {
"use strict";

function ueh() {
    return global.cnri && global.cnri._jse && global.cnri._jse.uncaughtExceptionHandler;
}

function setImmediate(fn) {
    var eventLoop = cnri._jse.threadLocalJavaScriptEventLoop.get();
    if (!eventLoop) throw { name: 'Error', message: 'setImmediate must be called on a JavaScriptEventLoop thread'};
    if (typeof fn === 'string' || Object.prototype.toString.call(fn) === '[object String]') {
        return eventLoop.setImmediate(ueh(), function () { eval(fn); });
    } else {
        var args = [].slice.call(arguments, 1, arguments.length);
        return eventLoop.setImmediate(ueh(), fn, Java.to(args, 'java.lang.Object[]'));
    }
}
global.setImmediate = setImmediate;

function clearImmediate(timeoutId) {
    if (timeoutId && typeof timeoutId.cancel === 'function') timeoutId.cancel(false);
}
global.clearImmediate = clearImmediate;

function setTimeout(fn, delay) {
    var eventLoop = cnri._jse.threadLocalJavaScriptEventLoop.get();
    if (!eventLoop) throw { name: 'Error', message: 'setTimeout must be called on a JavaScriptEventLoop thread'};
    if (typeof fn === 'string' || Object.prototype.toString.call(fn) === '[object String]') {
        return eventLoop.setTimeout(ueh(), function () { eval(fn); }, delay);
    } else {
        var args = [].slice.call(arguments, 2, arguments.length);
        return eventLoop.setTimeout(ueh(), fn, delay, Java.to(args, 'java.lang.Object[]'));
    }
}
global.setTimeout = setTimeout;

function clearTimeout(timeoutId) {
    if (timeoutId && typeof timeoutId.cancel === 'function') timeoutId.cancel(false);
}
global.clearTimeout = clearTimeout;

function setInterval(fn, delay) {
    var eventLoop = cnri._jse.threadLocalJavaScriptEventLoop.get();
    if (!eventLoop) throw { name: 'Error', message: 'setInterval must be called on a JavaScriptEventLoop thread'};
    if (typeof fn === 'string' || Object.prototype.toString.call(fn) === '[object String]') {
        return eventLoop.setInterval(ueh(), function () { eval(fn); }, delay);
    } else {
        var args = [].slice.call(arguments, 2, arguments.length);
        return eventLoop.setInterval(ueh(), fn, delay, Java.to(args, 'java.lang.Object[]'));
    }
}
global.setInterval = setInterval;

function clearInterval(timeoutId) {
    if (timeoutId && typeof timeoutId.cancel === 'function') timeoutId.cancel(false);
}
global.clearInterval = clearInterval;

})(this);