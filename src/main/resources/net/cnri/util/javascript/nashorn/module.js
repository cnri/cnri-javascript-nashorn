function (parent, filename, requireModule, resolveFilename) {
    var module = {
        parent: parent,
        filename: filename,
        exports: {},
        children: [],
        id: filename,
        loaded: false
    };
    var require = function (id) {
        var childModule = requireModule(module, id);
        if (!childModule) {
            var error = new Error("Cannot find module '" + id + "'");
            error.code = 'MODULE_NOT_FOUND';
            throw error;
        }
        return childModule.exports;
    };
    require.main = parent ? parent.require.main : module;
    require.resolve = function (id) {
        var res = resolveFilename(module, id);
        if (!res) {
            var error = new Error("Cannot find module '" + id + "'");
            error.code = 'MODULE_NOT_FOUND';
            throw error;
        }
        return res;
    };
    require.cache = cnri._jse.moduleCache;
    module.require = require;
    if (parent) parent.children.push(module);
    return module;
}