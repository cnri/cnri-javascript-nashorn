//# sourceURL=console.js
(function (global) {
"use strict";

if (global.console) return;

function fixUndefinedAndNull(s) {
    if (s === null || s === undefined) return '' + s;
    return s;
}

function format(fmt) {
    if (typeof fmt !== 'string' && Object.prototype.toString.call(fmt) !== '[object String]') {
        return [].slice.call(arguments).map(fixUndefinedAndNull).join(' ');
    }
    var re = /%[%jds]/g;
    var args = [].slice.call(arguments, 1);
    var res = fmt.replace(re, function(match) {
        if (match === '%%') return '%';
        else if (args.length > 0) {
            var arg = args.shift();
            if (match === '%s') return '' + arg; 
            if (match === '%d') return +arg;
            if (match === '%j') return JSON.stringify(arg);
            return match;
        } else {
            return match;
        }
    });
    if (args.length > 0) {
        res += ' ' + args.map(fixUndefinedAndNull).join(' ');
    }
    return res;
}

var console = {};

console.assert = function assert(value) {
    if (!value) {
        throw { name: 'AssertionError', message: format([].slice.call(arguments, 1)) };
    }
};

console.time = function () { }

console.timeEnd = function () { }

console.dir = function (obj) { console.log(obj); };

console.log = function () {
    if (typeof cnri !== 'undefined' && cnri._jse && cnri._jse.logger && typeof cnri._jse.logger.info === 'function') {
        cnri._jse.logger.info(format.apply(null, arguments));
    } else {
        print(format.apply(null, arguments));
    }
};

console.error = function () {
    if (typeof cnri !== 'undefined' && cnri._jse && cnri._jse.logger && typeof cnri._jse.logger.error === 'function') {
        cnri._jse.logger.error(format.apply(null, arguments));
    } else {
        console.log.apply(null, arguments);
    }
};

console.info = console.log;

console.trace = console.error;

console.warn = function () {
    if (typeof cnri !== 'undefined' && cnri._jse && cnri._jse.logger && typeof cnri._jse.logger.warn === 'function') {
        cnri._jse.logger.warn(format.apply(null, arguments));
    } else {
        console.log.apply(null, arguments);
    }
};

global.console = global.console || console;

})(this);